import 'mock_data/books.dart';
import 'package:mr_yellow/model/book.dart';
import 'package:shelf/shelf_io.dart';
import 'package:rpc/rpc.dart';
import 'package:shelf_rpc/shelf_rpc.dart';


main() async {
  var apiServer = new ApiServer(prettyPrint: true)
    ..addApi(new BookApi());

  var server = await serve(createRpcHandler(apiServer), 'localhost', 3333);
  print('serving api at: ${server.address}:${server.port}');
}

@ApiClass(name: 'api', version: 'v1')
class BookApi {
  @ApiMethod(path: 'books/{id}')
  Book getOne(int id) {
    return books_mock[id];
  }

  @ApiMethod(path: 'books')
  List<Book> getAll() {
    return books_mock.values.toList();
  }

//  @ApiMethod(path: 'books', method: 'POST')
//  Book create() {
//    return new Book()..id = 1;
//  }
//  @ApiMethod(path: 'books/{id}', method: 'PUT')
//  Book update(int id) {
//    return new Book()..id = id;
//  }
}
