import 'package:mr_yellow/model/author.dart';
import 'package:mr_yellow/model/book.dart';

final books_mock = {
  1: new Book()
    ..okladkaUrl = "http://lorempixel.com/200/250/"
    ..author = new Author.Params("Dick Tracy")
    ..title = "King Arthur and Robin Hood"
    ..dbIndex = 1,

  2: new Book()
    ..okladkaUrl = "http://lorempixel.com/200/250/"
    ..author = new Author.Params("Dick Tracy Fancy Look")
    ..title = "King Arthur"
    ..dbIndex = 2
};
