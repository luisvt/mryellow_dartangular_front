import 'dart:convert';
import 'dart:io';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';
import 'package:shelf_proxy/shelf_proxy.dart';
import 'package:shelf/src/message.dart' show getBody;

main() async {
  _startPubServe();
  var server = await serve(handler, 'localhost', 9080);
  print('Proxying at http://${server.address.host}:${server.port}');
}

handler(Request request) {
  print('request.url.path: ${request.url.path}');
  if (request.url.path.startsWith('api')) {
    print('proxying to: http://localhost:3333/api');
    return proxyHandler('http://localhost:3333/api')(request);
  }

  var handler = proxyHandler('http://localhost:8080');
  if (new RegExp(r'\.(css|dart|html|png|ttf|otf|TTF)$').hasMatch(request.url.path)) {
    print('proxyiing to: http://localhost:8080');
    return handler(request);
  }

  print('proxying to: http://localhost:8080/index.html');
  return handler(new Request(
      request.method,
      Uri.parse('http://localhost:8080/index.html'),
      protocolVersion: request.protocolVersion,
      headers: request.headers,
      handlerPath: request.handlerPath,
      body: getBody(request),
      encoding: request.encoding,
      context: request.context));
}

_startPubServe() async {
  String executable = Platform.isWindows ? 'pub.bat' : 'pub';
  var process = await Process.start(executable, ['serve', '--port', '8080']);
  process.stdout.transform(UTF8.decoder).listen(print);
}
