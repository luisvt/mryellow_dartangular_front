// Copyright (c) 2017. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.
library mryello_dartangular_front;

import 'package:angular2/angular2.dart';
import 'package:angular2/platform/browser.dart';
import 'package:http/browser_client.dart';
import 'package:mr_yellow/app_component.dart';
import 'package:http/http.dart';
import 'package:dson/dson.dart';

import 'package:mr_yellow/model/author.dart';
import 'package:mr_yellow/model/book.dart';
import 'package:mr_yellow/model/page.dart';

part 'main.g.dart';

void main() {
  _initMirrors();
  bootstrap(AppComponent, [
    provide(Client, useFactory: () => new BrowserClient(), deps: [])
  ]);
}
