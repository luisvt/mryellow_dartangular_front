// GENERATED CODE - DO NOT MODIFY BY HAND

part of mryello_dartangular_front;

// **************************************************************************
// Generator: InitMirrorsGenerator
// Target: library mryello_dartangular_front
// **************************************************************************

_initMirrors() {
  initClassMirrors({
    Author: AuthorClassMirror,
    Book: BookClassMirror,
    Page: PageClassMirror
  });
  initFunctionMirrors({});
}
