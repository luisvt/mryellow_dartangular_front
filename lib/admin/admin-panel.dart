import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';
import 'nav-bar-admin.dart';
import '../footer1.dart';
import '../reader/book-read-panel.dart';
import '../reader/library.dart';

@Component(
  directives: const [
    ROUTER_DIRECTIVES, COMMON_DIRECTIVES, NavBarAdmin, Footer1
  ],
  selector: 'admin-panel',
  templateUrl: 'admin-panel.html',
)
@RouteConfig(const [
  const Route(path: 'book/:id', name: 'Book', component: BookReadPanel, useAsDefault: false),
  const Route(path: '/', name: 'Library', component: Library, useAsDefault: true)
])
class AdminPanel {
/*
router: Router;
  configureRouter(config: RouterConfiguration, router: Router) {
    config.map([
      { route: ['book/:id/'],  moduleId: './admin/book-edit-panel',  name: 'BookEditPanel', title: 'book edit panel'},
      { route: ['','main'],  moduleId: './admin/main',  name: 'Main', title: 'admin', nav: true },
      { route: ['library/:mode?'],  moduleId: 'library',  name: 'library', title: 'library',nav: true, href:'#/reader/library/admin'}
    ]);

    this.router = router;
    console.log(this.router);
  }
 */
}
