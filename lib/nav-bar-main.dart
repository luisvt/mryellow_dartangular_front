import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

@Component(
 directives: const [
    ROUTER_DIRECTIVES, COMMON_DIRECTIVES
  ],
 
  selector: 'nav-bar-main',
  templateUrl: 'nav-bar-main.html',
)
class NavBarMain {
  var name = 'nav-bar-main';
}