// GENERATED CODE - DO NOT MODIFY BY HAND

part of model.book;

// **************************************************************************
// Generator: DsonGenerator
// Target: class Book
// **************************************************************************

abstract class _$BookSerializable extends SerializableMap {
  int get id;
  String get rawcontent;
  String get title;
  Author get author;
  Page get curPage;
  List<Page> get pages;
  String get okladkaUrl;
  String get temp;
  int get dbIndex;
  int get numberOfPages;
  void set id(int v);
  void set rawcontent(String v);
  void set title(String v);
  void set author(Author v);
  void set curPage(Page v);
  void set pages(List<Page> v);
  void set okladkaUrl(String v);
  void set temp(String v);
  void set dbIndex(int v);
  dynamic setCurPage(int numer);
  dynamic nextPage();
  dynamic prevPage();

  operator [](Object key) {
    switch (key) {
      case 'id':
        return id;
      case 'rawcontent':
        return rawcontent;
      case 'title':
        return title;
      case 'author':
        return author;
      case 'curPage':
        return curPage;
      case 'pages':
        return pages;
      case 'okladkaUrl':
        return okladkaUrl;
      case 'temp':
        return temp;
      case 'dbIndex':
        return dbIndex;
      case 'numberOfPages':
        return numberOfPages;
      case 'setCurPage':
        return setCurPage;
      case 'nextPage':
        return nextPage;
      case 'prevPage':
        return prevPage;
    }
    throwFieldNotFoundException(key, 'Book');
  }

  operator []=(Object key, value) {
    switch (key) {
      case 'id':
        id = value;
        return;
      case 'rawcontent':
        rawcontent = value;
        return;
      case 'title':
        title = value;
        return;
      case 'author':
        author = value;
        return;
      case 'curPage':
        curPage = value;
        return;
      case 'pages':
        pages = value;
        return;
      case 'okladkaUrl':
        okladkaUrl = value;
        return;
      case 'temp':
        temp = value;
        return;
      case 'dbIndex':
        dbIndex = value;
        return;
    }
    throwFieldNotFoundException(key, 'Book');
  }

  Iterable<String> get keys => BookClassMirror.fields.keys;
}

_Book__Constructor(params) => new Book();

const $$Book_fields_id = const DeclarationMirror(type: int);
const $$Book_fields_rawcontent = const DeclarationMirror(type: String);
const $$Book_fields_title = const DeclarationMirror(type: String);
const $$Book_fields_author = const DeclarationMirror(type: Author);
const $$Book_fields_curPage = const DeclarationMirror(type: Page);
const $$Book_fields_pages = const DeclarationMirror(type: const [List, Page]);
const $$Book_fields_okladkaUrl = const DeclarationMirror(type: String);
const $$Book_fields_temp = const DeclarationMirror(type: String);
const $$Book_fields_dbIndex = const DeclarationMirror(type: int);
const $$Book_fields_numberOfPages =
    const DeclarationMirror(type: int, isFinal: true);

const BookClassMirror = const ClassMirror(name: 'Book', constructors: const {
  '': const FunctionMirror(parameters: const {}, call: _Book__Constructor)
}, fields: const {
  'id': $$Book_fields_id,
  'rawcontent': $$Book_fields_rawcontent,
  'title': $$Book_fields_title,
  'author': $$Book_fields_author,
  'curPage': $$Book_fields_curPage,
  'pages': $$Book_fields_pages,
  'okladkaUrl': $$Book_fields_okladkaUrl,
  'temp': $$Book_fields_temp,
  'dbIndex': $$Book_fields_dbIndex,
  'numberOfPages': $$Book_fields_numberOfPages
}, getters: const [
  'id',
  'rawcontent',
  'title',
  'author',
  'curPage',
  'pages',
  'okladkaUrl',
  'temp',
  'dbIndex',
  'numberOfPages'
], setters: const [
  'id',
  'rawcontent',
  'title',
  'author',
  'curPage',
  'pages',
  'okladkaUrl',
  'temp',
  'dbIndex'
], methods: const {
  'setCurPage': const FunctionMirror(
      name: 'setCurPage',
      returnType: dynamic,
      parameters: const {'numer': const DeclarationMirror(type: int)},
      annotations: const [ignore]),
  'nextPage': const FunctionMirror(
      name: 'nextPage', returnType: dynamic, annotations: const [ignore]),
  'prevPage': const FunctionMirror(
      name: 'prevPage', returnType: dynamic, annotations: const [ignore])
});
