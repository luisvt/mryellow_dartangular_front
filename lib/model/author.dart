library model.author; // this line is needed for the generator

import 'package:dson/dson.dart';

part 'author.g.dart'; // this line is needed for the generator

@serializable
class Author extends _$AuthorSerializable {

  int id;
  String name;
  DateTime date_birth;

  Author.Params(this.name);

  Author() {

  }
}
