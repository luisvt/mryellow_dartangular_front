// GENERATED CODE - DO NOT MODIFY BY HAND

part of model.page;

// **************************************************************************
// Generator: DsonGenerator
// Target: class Page
// **************************************************************************

abstract class _$PageSerializable extends SerializableMap {
  String get content;
  String get type;
  int get number;
  void set content(String v);
  void set type(String v);
  void set number(int v);

  operator [](Object key) {
    switch (key) {
      case 'content':
        return content;
      case 'type':
        return type;
      case 'number':
        return number;
    }
    throwFieldNotFoundException(key, 'Page');
  }

  operator []=(Object key, value) {
    switch (key) {
      case 'content':
        content = value;
        return;
      case 'type':
        type = value;
        return;
      case 'number':
        number = value;
        return;
    }
    throwFieldNotFoundException(key, 'Page');
  }

  Iterable<String> get keys => PageClassMirror.fields.keys;
}

_Page__Constructor(params) => new Page();

const $$Page_fields_content = const DeclarationMirror(type: String);
const $$Page_fields_type = const DeclarationMirror(type: String);
const $$Page_fields_number = const DeclarationMirror(type: int);

const PageClassMirror = const ClassMirror(name: 'Page', constructors: const {
  '': const FunctionMirror(parameters: const {}, call: _Page__Constructor)
}, fields: const {
  'content': $$Page_fields_content,
  'type': $$Page_fields_type,
  'number': $$Page_fields_number
}, getters: const [
  'content',
  'type',
  'number'
], setters: const [
  'content',
  'type',
  'number'
]);
