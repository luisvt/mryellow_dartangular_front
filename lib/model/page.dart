library model.page;

import 'package:dson/dson.dart';

part 'page.g.dart';

@serializable
class Page extends _$PageSerializable {
  String content;

  /// <text, image>
  String type = 'text';

  int number;
}
