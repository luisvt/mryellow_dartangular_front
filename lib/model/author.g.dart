// GENERATED CODE - DO NOT MODIFY BY HAND

part of model.author;

// **************************************************************************
// Generator: DsonGenerator
// Target: class Author
// **************************************************************************

abstract class _$AuthorSerializable extends SerializableMap {
  int get id;
  String get name;
  DateTime get date_birth;
  void set id(int v);
  void set name(String v);
  void set date_birth(DateTime v);

  operator [](Object key) {
    switch (key) {
      case 'id':
        return id;
      case 'name':
        return name;
      case 'date_birth':
        return date_birth;
    }
    throwFieldNotFoundException(key, 'Author');
  }

  operator []=(Object key, value) {
    switch (key) {
      case 'id':
        id = value;
        return;
      case 'name':
        name = value;
        return;
      case 'date_birth':
        date_birth = value;
        return;
    }
    throwFieldNotFoundException(key, 'Author');
  }

  Iterable<String> get keys => AuthorClassMirror.fields.keys;
}

_Author_Params_Constructor(params) => new Author.Params(params['name']);
_Author__Constructor(params) => new Author();

const $$Author_fields_id = const DeclarationMirror(type: int);
const $$Author_fields_name = const DeclarationMirror(type: String);
const $$Author_fields_date_birth = const DeclarationMirror(type: DateTime);

const AuthorClassMirror =
    const ClassMirror(name: 'Author', constructors: const {
  'Params': const FunctionMirror(
      parameters: const {'name': const DeclarationMirror(type: String)},
      call: _Author_Params_Constructor),
  '': const FunctionMirror(parameters: const {}, call: _Author__Constructor)
}, fields: const {
  'id': $$Author_fields_id,
  'name': $$Author_fields_name,
  'date_birth': $$Author_fields_date_birth
}, getters: const [
  'id',
  'name',
  'date_birth'
], setters: const [
  'id',
  'name',
  'date_birth'
]);
