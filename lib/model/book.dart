library model.book; // this line is needed for the generator

import 'package:dson/dson.dart';
import 'author.dart';
import 'page.dart';

part 'book.g.dart'; // this line is needed for the generator

@serializable
class Book extends _$BookSerializable {
  int id;
  String rawcontent;
  String title;
  Author author;
  Page curPage;
  List<Page> pages;
  String okladkaUrl;
  String temp;
  int dbIndex;

  @ignore
  setCurPage(int numer) {
    this.curPage = this.pages[numer - 1];
  }

  @ignore
  int get numberOfPages => this.pages?.length ?? 0;

  @ignore
  nextPage() {
    if (this.curPage.number == this.numberOfPages) {
      this.curPage = this.pages[0];
    } else {
      this.curPage =
      this.pages[this.curPage.number]; //next page index is equal current Page number (array begins with 0)
    }
    //  console.log(this);
  }

  @ignore
  prevPage() {
    if (this.curPage.number == 1) {
      this.curPage = this.pages[this.numberOfPages - 1];
    } else {
      this.curPage = this.pages[this.curPage.number - 2];
    }
    //console.log(this);
  }
}
