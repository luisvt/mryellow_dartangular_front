import 'dart:async';
import 'dart:convert';

import 'package:angular2/angular2.dart';
import 'package:dson/dson.dart';
import 'package:http/http.dart';

import '../model/book.dart';


@Injectable()
class BookService {
  static final _headers = {'Content-Type': 'application/json'};
  static const _apiUrl = 'api/v1/books'; // URL to web API
  final Client _http;

  BookService(this._http);

  Future<List<Book>> getAll() async {
    try {
      final response = await _http.get(_apiUrl);
      print(response.body);
      List<Book> books = fromJson(response.body, [List, Book]);

      return books;
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<Book> create(String name) async {
    try {
      final response = await _http.post(_apiUrl,
          headers: _headers, body: JSON.encode({'name': name}));
      return fromJson(response.body, Book);
    } catch (e) {
      throw _handleError(e);
    }
  }

  Exception _handleError(dynamic e) {
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }
}
