// Copyright (c) 2017. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/angular2.dart';
//import 'package:angular_components/angular_components.dart';
import 'package:angular2/router.dart';
import 'footer1.dart';
import 'nav-bar-main.dart';

@Component(
  directives: const [
    ROUTER_DIRECTIVES, COMMON_DIRECTIVES, Footer1, NavBarMain
  ],

  selector: 'main_page',
  templateUrl: 'main_page.html'
)
class MainPage {
  var name = 'Angular';
}
