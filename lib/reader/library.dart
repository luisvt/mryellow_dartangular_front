import 'dart:async';

import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import '../model/book.dart';
import '../services/book_service.dart';
import 'book-min.dart';

@Component(
  selector: 'library',
  templateUrl: 'library.html',
  providers: const [BookService],
  directives: const [
    ROUTER_DIRECTIVES, COMMON_DIRECTIVES, BookMin
  ],
)
class Library implements OnInit {
  final BookService _bookService;
  final RouteParams _routeParams;
  String errorMessage;
  List<Book> books = [];

  Library(this._routeParams, this._bookService);

  Future<Null> ngOnInit() => getBooks();

  Future<Null> getBooks() async {
    var _mode = _routeParams.get('mode');
    try {
      books = await _bookService.getAll();
    } catch (e) {
      errorMessage = e.toString();
    }
  }

//Todo
  Future<Null> addHero(String name) async {
    name = name.trim();
    if (name.isEmpty) return;
    try {
      books.add(await _bookService.create(name));
    } catch (e) {
      errorMessage = e.toString();
    }
  }
/*
activate(params){
    if(params.mode=="admin"){
        this.showAdminPanel = true;
        console.log(params.mode);
    }
    }
   */
}
