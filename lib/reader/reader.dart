import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';
import 'nav-bar-reader.dart';
import 'book-read-panel.dart';
import 'library.dart';
import '../admin/admin-panel.dart';

@Component(
  directives: const [
    ROUTER_DIRECTIVES, COMMON_DIRECTIVES, NavBarReader
  ],
  selector: 'reader',
  templateUrl: 'reader.html',
)
@RouteConfig(const [
  const Route(path: 'admin/...', name: 'Admin', component: AdminPanel),
  const Route(path: 'book/:id/:page', name: 'BookReadPanel', component: BookReadPanel),
  const Route(path: 'library/:mode', name: 'Library', component: Library, useAsDefault: true)
])
class Reader {
}
