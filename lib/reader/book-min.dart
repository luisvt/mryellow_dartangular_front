import 'package:angular2/angular2.dart';
import '../model/book.dart';
import 'package:angular2/router.dart';

@Component(
 directives: const [ROUTER_DIRECTIVES, COMMON_DIRECTIVES  ],
  
  selector: 'book-min',
  templateUrl: 'book-min.html',
)


class BookMin {

    @Input() Book book;
    bool showAdminPanel;


}