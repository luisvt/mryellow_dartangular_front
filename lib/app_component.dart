// Copyright (c) 2017. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/angular2.dart';
import 'main_page.dart';
import 'package:angular2/router.dart';
import 'reader/reader.dart';
import 'admin/admin-panel.dart';
import 'not_found_component.dart';
@Component(
  directives: const [ROUTER_DIRECTIVES],
  providers: const [ROUTER_PROVIDERS],
  selector: 'my-app',
  template: '<router-outlet></router-outlet>',
)
@RouteConfig(const [
  const Route(path: '/', name: 'MainPage', component: MainPage, useAsDefault: true),
  const Route(path: 'admin/...', name: 'Admin', component: AdminPanel),
  const Route(path: 'reader/...', name: 'ReaderPanel', component: Reader),
  const Route(path: '/**', name: 'NotFound', component: NotFoundComponent),
])
class AppComponent {
  var name = 'Angular';
}
